import 'bootstrap/dist/css/bootstrap.min.css';
import CarListFilter from './components/CarListFilter';

function App() {
  return (
    <div>
      <CarListFilter />
    </div>
  );
}

export default App;
