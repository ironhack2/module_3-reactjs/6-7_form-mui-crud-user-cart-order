import { useState } from 'react';
import { Input, Label, Form, FormGroup, Col, ButtonGroup, Button, Card, CardBody, CardTitle, CardSubtitle } from 'reactstrap';


function CarListFilter() {
    const carList = [
        {
            name: "BMW M6",
            url:
                "https://mediapool.bmwgroup.com/cache/P9/201411/P90169551/P90169551-the-new-bmw-m6-coup-exterior-12-2014-600px.jpg",
            release_year: 2020
        },
        {
            name: "VW Polo",
            url:
                "https://cdn.euroncap.com/media/30740/volkswagen-polo-359-235.jpg?mode=crop&width=359&height=235",
            release_year: 2018
        },
        {
            name: "Audi S6",
            url:
                "https://www.motortrend.com/uploads/sites/5/2020/03/6-2020-audi-s6.jpg?fit=around%7C875:492.1875",
            release_year: 2020
        },
        {
            name: "BMW M2",
            url:
                "https://imgd.aeplcdn.com/0x0/cw/ec/37092/BMW-M2-Exterior-141054.jpg?wm=0",
            release_year: 2019
        },
        {
            name: "Audi A3",
            url: "https://cdn.motor1.com/images/mgl/BEooZ/s3/2021-audi-s3.jpg",
            release_year: 2019
        }
    ];

    const [branh, setBranh] = useState(carList)

    const selectBranh = (event) => {
        if(event.target.value === "All"){
            setBranh(carList)
            return
        }
        let filter = carList.filter(item => item.name.includes(event.target.value))
        setBranh(filter)
    }

    const btnYearClick = (param) => {
        console.log(param)
        let filter = carList.filter(item => item.release_year == param)
        setBranh(filter)
    }
    

    return (
        <div className="container">
            <div className="row w-25 m-auto">
                <Form >
                    <FormGroup row>
                        <Label sm={5}>Filter by Branh</Label>
                        <Col sm={7}>
                            <Input type="select" onChange={selectBranh}>
                                <option>All</option>
                                <option>BMW</option>
                                <option>VW</option>
                                <option>Audi</option>
                            </Input>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
            <div className="row w-25 m-auto">
                <Form >
                    <FormGroup row>
                        <Label sm={12} className="text-center">Filter by Year</Label>
                        <ButtonGroup>
                            <Button onClick={() => {btnYearClick(2018)}}>2018</Button>
                            <Button onClick={() => {btnYearClick(2019)}}>2019</Button>
                            <Button onClick={() => {btnYearClick(2020)}}>2020</Button>
                        </ButtonGroup>
                    </FormGroup>
                </Form>
            </div>
            <div className="row w-100 m-auto justify-content-center">
                {
                    branh.map((element, index) => {
                        return (
                            <Card key={index} className='w-25 m-4'>
                                <CardBody>
                                    <CardTitle tag="h5">
                                        {element.name}
                                    </CardTitle>
                                    <CardSubtitle
                                        className="mb-2 text-muted"
                                        tag="h6"
                                    >
                                        {element.release_year}
                                    </CardSubtitle>
                                </CardBody>
                                <img
                                    alt="Card image cap"
                                    src={element.url}
                                    width="100%"
                                />
                            </Card>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default CarListFilter