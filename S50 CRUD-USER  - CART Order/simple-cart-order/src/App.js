import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import { Container, Row } from "reactstrap";

import OrderDetail from "./components/OrderDetail";

const mobileList = [
  {
    name: "IPhone X",
    price: 900,
  },
  {
    name: "Samsung S9",
    price: 800,
  },
  {
    name: "Nokia 8",
    price: 650,
  },
];

function App() {
  const [cost, setCost] = useState(0);

  const addCost = (value) => {
    setCost(cost + value);
  };

  return (
    <Container className="mt-3">
      <Row>
        <h3>Mobile Order</h3>
      </Row>
      <Row>
        {mobileList.map((mobile, index) => {
          return (
            <OrderDetail
              key={index}
              nameProp={mobile.name}
              priceProp={mobile.price}
              addTotalProp={addCost}
            />
          );
        })}
      </Row>
      <Row>
        <b>Total: {cost} USD</b>
      </Row>
    </Container>
  );
}

export default App;
